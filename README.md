# Orthogonal Bidding Strategy Extended (OBS+)

A modularized open MAS implementation in
[JaCaMo](https://github.com/jacamo-lang/jacamo) of OBS+ negotiation method.
